package com.payment.strategy.service;

import java.util.Map;

/**
 * created by YDW
 */
public abstract  class AbstractPayService implements PayStrategy {
    protected  String payType;


    public  void pay(String payType, Map<String,String> param){
        this.payType=payType;
        boolean isSuccess =prePay(param);

        if(isSuccess){
            System.out.println("成功提交支付");
        }else{
            System.out.println("未成功提交");
        }
     afterPay(param,true);
    }
}
