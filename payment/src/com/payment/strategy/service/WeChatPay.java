package com.payment.strategy.service;

import com.payment.strategy.annotation.Pay;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;

/**
 * created by YDW
 */
@Service
@Pay(value = "weCaht")
public class WeChatPay implements PayStrategy {

    @Override
    public boolean prePay(Map<String, String> param) {
        System.out.println("使用微信支付"+"入参"+param.toString());
        return true;
    }

    @Override
    public void afterPay(Map<String, String> param, boolean isSuccess) {
        System.out.printf("微信回调回调");
    }
}
