package com.payment.strategy.service;

import com.payment.strategy.annotation.Pay;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;

/**
 * created by YDW
 */

@Service
@Pay(value = "aliPay")
public class AliPay implements PayStrategy {

    /**
     * 扩展公共参数类 或者定义系统级别接口
     * @param param
     * @return
     */

    @Override
    public boolean prePay(Map<String, String> param) {
        System.out.println("使用支付宝支付"+"入参"+param.toString());
        return true;
    }

    @Override
    public void afterPay(Map<String, String> param, boolean isSuccess) {
        System.out.printf("支付宝回调回调");
    }
}
