package com.payment.strategy.service;

import com.payment.strategy.factory.PayStrategyFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * created by YDW
 */
@Service
public class PayServiceImpl extends  AbstractPayService{

    private static final Logger logger = LoggerFactory.getLogger(PayServiceImpl.class);


    @Override
    public boolean prePay(Map<String, String> param) {
        PayStrategy payStrategy = PayStrategyFactory.getPayStrategy(this.payType);
        if(payStrategy==null){
            logger.info("暂无支付策略");
            return false;
        }else{
            return  payStrategy.prePay(param);
        }
    }

    @Override
    public void afterPay(Map<String, String> param, boolean isSuccess) {
        PayStrategy payStrategy = PayStrategyFactory.getPayStrategy(this.payType);
        payStrategy.afterPay(param, true);
    }
}
