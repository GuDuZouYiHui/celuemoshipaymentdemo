package com.payment.strategy.service;

import java.math.BigDecimal;
import java.util.Map;

/**
 * created by YDW
 */
public interface PayStrategy {

    boolean prePay(Map<String,String> param);

    void afterPay(Map<String,String> param,boolean isSuccess);
}
