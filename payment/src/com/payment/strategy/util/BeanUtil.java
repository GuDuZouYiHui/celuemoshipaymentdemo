package com.payment.strategy.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * created by YDW
 */
@Component
public class BeanUtil  implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        BeanUtil.applicationContext = applicationContext;
    }

    /**
     * 获取策略类
     * @param clazz
     * @return
     */
    public static Object getBean(Class clazz){
         return  applicationContext.getBean(clazz);
    }
}
