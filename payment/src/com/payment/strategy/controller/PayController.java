package com.payment.strategy.controller;

import com.payment.strategy.service.AbstractPayService;
import com.payment.strategy.service.PayServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;

/**
 * created by YDW
 */
@Controller
@RequestMapping("/index")
public class PayController {

    @Autowired
    PayServiceImpl payService;

    @Autowired
    AbstractPayService abstractPayService;

    @GetMapping("/get")
    public String index(@RequestParam(name = "payType") String payType){
        payService.pay(payType,new HashMap<>());
        return  null;
    }
}
